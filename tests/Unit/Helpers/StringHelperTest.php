<?php

namespace Tests\Unit\Helpers;

use App\Iprice\Helpers\StringHelper;
use Tests\TestCase;

class StringHelperTest extends TestCase
{
    //test string converted to upper and lower case (each character)
    public function test_string_to_upper_lower_characters(): void
    {
        $text = 'new statement tested';

        //inject the string helper to framework container
        $stringHelperObj = $this->app->make(StringHelper::class)->stringUpperLowerCharacters($text);

        $this->assertEquals('nEw sTaTeMeNt tEsTeD', $stringHelperObj);
    }
}
