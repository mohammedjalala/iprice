<?php

namespace Tests\Feature\UpperLowerString;

use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpperLowerStringTest extends TestCase
{
    use WithFaker;

    //Test upper case command with string input
    public function test_string_converted_to_upper_lower_successfully(): void
    {
        $text = 'this statement for testing';

        $expectedText = 'tHiS StAtEmEnT FoR TeStInG';

        $this->artisan('string:upper-lower')
            ->expectsQuestion('Please enter the string that you want to convert to upper and lower case!', $text)
            ->expectsOutput('String converted to upper and lower case : '. $expectedText)
            ->assertExitCode(0);
    }

    //Test upper case command with wrong input
    public function test_string_converted_to_upper_lower_failed(): void
    {
        $number = $this->faker->numberBetween(0, 10000);

        $this->artisan('string:upper-lower')
            ->expectsQuestion('Please enter the string that you want to convert to upper and lower case!', $number)
            ->expectsOutput('The text given is not string. please try again.')
            ->assertExitCode(0);
    }
}
