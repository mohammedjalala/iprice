<?php

namespace Tests\Feature\UppercaseString;

use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UppercaseStringTest extends TestCase
{
    use WithFaker;

    //Test upper case command with string input
    public function test_text_to_upper_case_command_successfully(): void
    {
        $text = $this->faker->sentence();

        $this->artisan('string:upper-case')
            ->expectsQuestion('Please enter your string!', $text)
            ->expectsOutput('Text converted to uppercase : '. strtoupper($text))
            ->assertExitCode(0);
    }

    //Test upper case command with wrong input
    public function test_text_to_upper_case_command_failed(): void
    {
        $number = $this->faker->numberBetween(0, 10000);

        $this->artisan('string:upper-case')
            ->expectsQuestion('Please enter your string!', $number)
            ->expectsOutput('The text given is not string. please try again.')
            ->assertExitCode(0);
    }
}
