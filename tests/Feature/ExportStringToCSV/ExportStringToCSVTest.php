<?php

namespace ExportStringToCSV;

use Illuminate\Foundation\Testing\WithFaker;
use Maatwebsite\Excel\Facades\Excel;
use Tests\TestCase;

class ExportStringToCSVTest extends TestCase
{
    use WithFaker;

    //Test upper case command with string input
    public function test_string_converted_to_csv_successfully(): void
    {
        $text = $this->faker->sentence();

        $testingFileName = 'testing_file';

        //check if file exists, then delete first
        if (file_exists('/csv/'. $testingFileName. '.csv')) {
            unlink('/csv/'. $testingFileName. '.csv');
        }

        $this->artisan('string:export-to-csv')
            ->expectsQuestion('Please enter the string that you want to convert to CSV!', $text)
            ->expectsQuestion('Please enter the CSV file to save it in filesystem!', $testingFileName)
            ->expectsOutput('CSV created!')
            ->assertExitCode(0);
    }

    //test if the csv file generate
    public function test_csv_file_generated()
    {
        $fileName = 'testing_file.csv';

        Excel::fake();

        if (!file_exists('/storage/csv/'. $fileName)) {
            $this->assertFalse(false);
        }

        $this->assertTrue(true);
    }
}
