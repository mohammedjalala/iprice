<?php

namespace App\Iprice\Traits;

trait StringValidator
{
    /**
     * validate string if not numeric for commands
     *
     * @param string $text
     * @return bool
     */
    public function isInputString(string $text): bool
    {
        //check if the text entered in the command is not number
        if (is_numeric($text)) {
            $this->error('The text given is not string. please try again.');

            return false;
        }

        return true;
    }
}
