<?php

namespace App\Iprice\Services;

use App\Iprice\Exports\StringCharactersExport;
use Maatwebsite\Excel\Facades\Excel;

class CSVExportService
{
    /**
     * export array to csv using excel package
     *
     * @param array $array
     * @param string $fileName
     * @return bool
     */
    public function exportArrayToCSV(array $array, string $fileName)
    {
        return Excel::store(new StringCharactersExport($array), $fileName.'.csv');
    }
}
