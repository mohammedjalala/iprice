<?php

namespace App\Iprice\Exports;

use Maatwebsite\Excel\Concerns\FromArray;

class StringCharactersExport implements FromArray
{
    private $inputCharacters;

    /**
     * StringCharactersExport constructor.
     *
     * @param array $array
     */
    public function __construct(array $array)
    {
        $this->inputCharacters = $array;
    }

    /**
     * return the array needed for CSV
     *
     * @return array|array[]
     */
    public function array(): array
    {
        return [$this->inputCharacters];
    }
}
