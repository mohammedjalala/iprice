<?php

namespace App\Iprice\Helpers;

class StringHelper
{
    /**
     * convert string to upper and lower case for each string char.
     *
     * @param string $text
     * @return string
     */
    public function stringUpperLowerCharacters(string $text): string
    {
        $textArray = str_split($text);

        foreach ($textArray as $key => $value) {
            if ($key % 2 === 0) { continue; }

            $textArray[$key] = strtoupper($value);
        }

        return implode('', $textArray);
    }
}
