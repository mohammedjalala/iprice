<?php

namespace App\Commands;

use App\Iprice\Traits\StringValidator;
use LaravelZero\Framework\Commands\Command;

class UppercaseStringCommand extends Command
{
    use StringValidator;

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'string:upper-case';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Convert string to uppercase and output the string to console';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //get the user input
        $text = $this->ask('Please enter your string!');

        //validate string, is if user enter number will show error message
        $this->isInputString($text);

        //
        $this->info('Text converted to uppercase : '. strtoupper($text));
    }
}
