<?php

namespace App\Commands;

use App\Iprice\Helpers\StringHelper;
use App\Iprice\Traits\StringValidator;
use LaravelZero\Framework\Commands\Command;

class UpperLowerStringCommand extends Command
{
    use StringValidator;
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'string:upper-lower';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'converts the string to alternate upper and lower case and outputs it to console';

    /**
     * Execute the console command.
     *
     * @param StringHelper $stringHelper
     */
    public function handle(StringHelper $stringHelper): void
    {
        //get the user input
        $text = $this->ask('Please enter the string that you want to convert to upper and lower case!');

        //validate string, is if user enter number will show error message
        $this->isInputString($text);

        $this->info('String converted to upper and lower case : '. $stringHelper->stringUpperLowerCharacters($text));
    }
}
