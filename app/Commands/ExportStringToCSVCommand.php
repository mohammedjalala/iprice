<?php

namespace App\Commands;

use App\Iprice\Services\CSVExportService;
use LaravelZero\Framework\Commands\Command;

class ExportStringToCSVCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'string:export-to-csv';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'creates a CSV file from the string you entered by making each character in a column.';

    /**
     * Execute the console command.
     *
     * @param CSVExportService $CSVExportService
     */
    public function handle(CSVExportService $CSVExportService): void
    {
        //get the user input
        $text = $this->ask('Please enter the string that you want to convert to CSV!');

        $fileName = $this->ask('Please enter the CSV file to save it in filesystem!');

        //exporting the given string to csv
        $CSVExportService->exportArrayToCSV(str_split($text), self::getFormattedFileName($fileName));

        $this->info('CSV created!');
    }

    /**
     * get formatted file name
     *
     * @param string $name
     * @param string $delimiter
     * @return string
     */
    private static function getFormattedFileName(string $name, $delimiter = '_'): string
    {
        $fullText = trim($name);

        $sumText = explode(' ',$fullText);

        return implode($delimiter, $sumText);
    }
}
