

<h4> Assignment</h4>

This small app build using [Laravel Zero](https://laravel-zero.com/). its small framework for building cli apps in PHP

------

## how to run the app locally

- clone the repository using git clone {app_url} under folder iprice for example.
- cd iprice and run composer update.
- run php iprice => to see the full list of commands available. you will see something like this with extra commands.<br><br>
<code> 
   USAGE: iprice <command> [options] [arguments]
 
   test                 Run the application tests<br>
 
   app:rename           Set the application name<br>
 
   make:command         Create a new command<br>
   string:export-to-csv creates a CSV file from the string you entered by making each character in a column.<br>
   string:upper-case    Convert string to uppercase and output the string to console<br>
   string:upper-lower   converts the string to alternate upper and lower case and outputs it to console<br>
</code>
- if you want to run convert text to upper case run <br>
    <code>php iprice string:upper-case</code>
    <br>then you will be asked to enter the text. if you enter number you will see error message.
    
- if you want to run convert text to upper and lower case run <br>
      <code>php iprice string:upper-lower</code> 
      <br>then you will be asked to enter the text. if you enter number you will see error message.
      
- if you want to run convert generate csv file from text you enter <br>
      <code>php iprice string:export-to-csv </code> 
      <br> you will be asked to enter the string , then you will be asked to enter the file name, so the filter will be stored under
      storage/csv/{file_name_you_entered_in_cli}.csv
      
- if you want to run the tests <br>
 <code>php iprice test </code> 
